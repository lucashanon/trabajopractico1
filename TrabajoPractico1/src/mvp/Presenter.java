package mvp;

import negocio.Juego;

public class Presenter {
	
	public static void iniciarCarga(int tamanio) {
		Juego.inciarTablero(tamanio);
	}

	public static void mezclarTablero() {
		Juego.cargarTablero();
	}

	public static void actualizarTablero(int x, int y) {
		Juego.actualizarTablero(x, y);	
	}

	public static boolean comprobarEstadoJuego() {
		return  Juego.comprobarEstado();
	}
	public static int cantidadDeTurnos() {
		return Juego.calcularCantidadTurnos();
	}
	
	public static int tamanioTablero() {
		return Juego.tamanio();
	}
	
	public static boolean estadoPosicion(int x, int y) {
		return Juego.estadoActual(x, y);
	}
	

}