package negocio;

public class Juego {
	private static boolean[][] tablero;
	
	public static void inciarTablero(int tamanio) {	
		tablero = new boolean[tamanio][tamanio];
		cargarTablero();
	}
	
	public static int tamanio() {
		return tablero.length;
	}
	
	public static boolean estadoActual(int x, int y){
		return tablero[x][y];
	}

	public static void cargarTablero() {
	    for (int i = 0; i < tablero.length; i++) {
	        for (int j = 0; j < tablero.length; j++) {
	            if (((int)(Math.random() * 100) + 1) % 2 == 0) {
	            	tablero[i][j] = true;
	            } else {
	            	tablero[i][j] = false;
	            }
	        }
	    }
	}
	public static void actualizarTablero(int x, int y) {
	    for (int i = 0; i < tablero.length; i++) {
	        for (int j = 0; j < tablero.length; j++) {
	            if (i==x || j==y) {
	            	tablero[i][j] = !tablero[i][j];
	            } 
	        }
	    }
	}
	public static boolean comprobarEstado() {
		for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero.length; j++) {
				if ( tablero[i][j]!=false) {
					return false;
				} 
			}
		}
		return true;
	}
	
	public static int calcularCantidadTurnos() {
		return ((tablero.length*tablero.length)*5)/4;
	}
	
}