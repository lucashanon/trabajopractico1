package interfaz;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.SwingConstants;

import mvp.Presenter;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import java.awt.Font;
import javax.swing.JLabel;

import java.awt.Color;
import javax.swing.JSeparator;

public class Menu {

	private JFrame frame;
	private static JTextField tamanioElegido;
	static JTextField nombreUsuario;
	private static int tam;
	private static JButton btnJugar;
	private static JLabel lblNewLabel;
	private static JSeparator separator;
	private static JLabel textoTamanioTablero;
	private static JLabel textoUsuario;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Menu window = new Menu();
					window.frame.setVisible(true);
					window.frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private void inicializarFrame() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.getContentPane().setFont(new Font("Times New Roman", Font.PLAIN, 18));
		frame.getContentPane().setForeground(new Color(173, 216, 230));
		frame.getContentPane().setBackground(new Color(230, 230, 250));
		frame.setTitle("INICIO");
		frame.setLocationRelativeTo(null);		
	}

	private void incializarComponentes() {
		lblNewLabel = new JLabel("Eliga su tablero");
		lblNewLabel.setBackground(new Color(0, 0, 0));
		lblNewLabel.setForeground(new Color(0, 0, 0));
		lblNewLabel.setFont(new Font("Times New Roman", Font.PLAIN, 39));
		lblNewLabel.setBounds(74, 11, 286, 39);
		
		tamanioElegido = new JTextField("");
		tamanioElegido.setHorizontalAlignment(SwingConstants.CENTER);
		tamanioElegido.setFont(new Font("Times New Roman", Font.PLAIN, 25));
		tamanioElegido.setBounds(357, 124, 67, 52);
		tamanioElegido.setColumns(10);

		separator = new JSeparator();
		separator.setBounds(-49, 61, 545, 2);
		
		nombreUsuario = new JTextField("");
		nombreUsuario.setHorizontalAlignment(SwingConstants.CENTER);
		nombreUsuario.setFont(new Font("Times New Roman", Font.PLAIN, 25));
		nombreUsuario.setColumns(10);
		nombreUsuario.setBounds(205, 69, 219, 52);
		
		btnJugar = new JButton("Jugar");
		btnJugar.setEnabled(false);
		btnJugar.setBackground(new Color(255, 255, 255));
		btnJugar.setFont(new Font("Times New Roman", Font.PLAIN, 26));
		btnJugar.setBounds(153, 212, 128, 38);
		
		textoTamanioTablero = new JLabel("Tamaño del Tablero");
		textoTamanioTablero.setForeground(Color.BLACK);
		textoTamanioTablero.setFont(new Font("Times New Roman", Font.PLAIN, 21));
		textoTamanioTablero.setBackground(Color.BLACK);
		textoTamanioTablero.setBounds(10, 132, 200, 39);
			
		textoUsuario = new JLabel("Nombre de Usuario");
		textoUsuario.setForeground(Color.BLACK);
		textoUsuario.setFont(new Font("Times New Roman", Font.PLAIN, 21));
		textoUsuario.setBackground(Color.BLACK);
		textoUsuario.setBounds(10, 74, 185, 39);	
	}

	private void cargarComponentes() {
		frame.getContentPane().add(lblNewLabel);
		frame.getContentPane().add(tamanioElegido);
		frame.getContentPane().add(separator);
		frame.getContentPane().add(nombreUsuario);
		frame.getContentPane().add(btnJugar);
		frame.getContentPane().add(textoTamanioTablero);
		frame.getContentPane().add(textoUsuario);
	}
	
	public static JTextField getNombreUsuario() {
		return nombreUsuario;
	}	

	private static void comprobarCampos() {
		if (!tamanioElegido.getText().isEmpty() && !nombreUsuario.getText().isBlank()) {
			btnJugar.setEnabled(true);
		}
		else {
			btnJugar.setEnabled(false);
		}
	}
	
	private void escucharBotonJugar() {
		btnJugar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					tam=Integer.parseInt(tamanioElegido.getText());			
					if(tam<2  ||tam >16){
						JOptionPane.showMessageDialog(null, "El numero tiene que ser mayor a 2 y menor a 16");
					}
					else{
						Presenter.iniciarCarga(tam);
						Tablero.main(null);
						frame.dispose();
					;}
					
				} catch (Exception e) {JOptionPane.showMessageDialog(null, "El tamaño del tablero tiene que ser un numero");}		
			}		
		});
	}
	
	private void escucharCampos() {
		tamanioElegido.getDocument().addDocumentListener(new javax.swing.event.DocumentListener() {
		    public void changedUpdate(javax.swing.event.DocumentEvent e) {
		        comprobarCampos();
		    }
		    public void removeUpdate(javax.swing.event.DocumentEvent e) {
		        comprobarCampos();
		    }
		    public void insertUpdate(javax.swing.event.DocumentEvent e) {
		        comprobarCampos();
		    }
		});
		nombreUsuario.getDocument().addDocumentListener(new javax.swing.event.DocumentListener() {
		    public void changedUpdate(javax.swing.event.DocumentEvent e) {
		        comprobarCampos();
		    }
		    public void removeUpdate(javax.swing.event.DocumentEvent e) {
		        comprobarCampos();
		    }
		    public void insertUpdate(javax.swing.event.DocumentEvent e) {
		        comprobarCampos();
		    }
		});
	}
	
	
	/**
	 * Create the application.
	 */
	public Menu() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		inicializarFrame();
		incializarComponentes();
		cargarComponentes();
		escucharBotonJugar();
		escucharCampos();
	
	}






}
