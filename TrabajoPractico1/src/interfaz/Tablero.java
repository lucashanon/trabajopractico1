package interfaz;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

import mvp.Presenter;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;

import javax.swing.SwingConstants;

import javax.swing.Timer;

public class Tablero {

	private JFrame frame;
	private static JLabel textoBienvenida;
	private static JButton btnMezclar = new JButton("Mezclar");
	private static JButton[][] tabBotones;
	private static int dimensionBoton;
	private static int espacioEntreBotones;
	private static int anchoBotones;
	private static int anchoFrame;
	private static int posicion;
	private static int turnosDisponibles;
	private JLabel textoMovimientosDisponibles;
	private JLabel turnos;
	private static JPanel panelTablero;
	private JLabel nombreJugador;
	private static JLabel cartelFinal;
	private JPanel panelFinal;
	private JPanel panelLuces;
	private static int tamanioTablero;
	private static JButton btnVolver;
	private JButton btnFinalizar;
	private Timer temporizador;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					Tablero window = new Tablero();
					window.frame.setVisible(true);
					//window.frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private void inicializarPanelBotones() {
		tamanioTablero=Presenter.tamanioTablero();
		tabBotones = new JButton[tamanioTablero][tamanioTablero];
		turnosDisponibles=Presenter.cantidadDeTurnos();
		
	}

	private void inicializarComponentes() {

		btnMezclar.setBounds(193, 561, 113, 23);
		
		textoMovimientosDisponibles = new JLabel("Movimientos disponibles:");
		textoMovimientosDisponibles.setFont(new Font("Comic Sans MS", Font.PLAIN, 16));
		textoMovimientosDisponibles.setBounds(266, 15, 184, 23);
		
		turnos = new JLabel("");
		turnos.setFont(new Font("Comic Sans MS", Font.PLAIN, 16));
		turnos.setBounds(458, 19, 33, 14);
		turnos.setText(String.valueOf(turnosDisponibles));
		
		nombreJugador = new JLabel(Menu.nombreUsuario.getText());
		nombreJugador.setFont(new Font("Comic Sans MS", Font.PLAIN, 16));
		nombreJugador.setBounds(106, 19, 150, 14);
		
		textoBienvenida = new JLabel("Buena suerte");
		textoBienvenida.setFont(new Font("Comic Sans MS", Font.PLAIN, 16));
		textoBienvenida.setBounds(10, 19, 100, 14);
		
		panelTablero = new JPanel();
		panelTablero.setBounds(0, 50, 500, 500);
		panelTablero.setLayout(null);
			
		panelLuces = new JPanel();
		panelLuces.setBounds(0, 0, 500, 500);
		panelTablero.add(panelLuces);
		panelLuces.setLayout(null);
		
		btnVolver = new JButton("Volver");
		btnVolver.setFont(new Font("Comic Sans MS", Font.PLAIN, 16));
		btnVolver.setBounds(10, 561, 113, 23);
				
		btnFinalizar = new JButton("Finalizar");
		btnFinalizar.setBounds(378, 561, 113, 23);
		btnFinalizar.setFont(new Font("Comic Sans MS", Font.PLAIN, 16));
	}
	
	private void inicializarPanelFinal() {
		panelFinal = new JPanel();
		panelFinal.setBackground(new Color(240, 240, 240));
		panelFinal.setBounds(0, 196, 500, 120);
		panelTablero.add(panelFinal);
		cartelFinal = new JLabel("");
		panelFinal.add(cartelFinal);
		cartelFinal.setBackground(Color.BLACK);
		cartelFinal.setForeground(Color.BLACK);
		cartelFinal.setVisible(false);
		panelFinal.setVisible(false);
		cartelFinal.setHorizontalAlignment(SwingConstants.CENTER);
		cartelFinal.setFont(new Font("Comic Sans MS", Font.PLAIN, 44));
		cartelFinal.setToolTipText("Ganaste");		
	}

	private void inicializarFrame() {
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(100, 100, 517, 637);
		frame.getContentPane().setForeground(new Color(0, 51, 0));
		frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(null);	
	}
	
	private void agregarComponentes() {
		frame.getContentPane().add(btnMezclar);
		frame.getContentPane().add(textoMovimientosDisponibles);
		frame.getContentPane().add(turnos);
		frame.getContentPane().add(nombreJugador);
		frame.getContentPane().add(textoBienvenida);
		frame.getContentPane().add(panelTablero);
		frame.getContentPane().add(btnVolver);
		frame.getContentPane().add(btnFinalizar);	
	}
	
	private void mostrarTablero() {
		int ejeY=posicion;
		for (int i = 0; i < Presenter.tamanioTablero(); i++) {
			int ejeX=posicion;
			for (int j = 0; j  < Presenter.tamanioTablero(); j++) {
				tabBotones[i][j]=new JButton("");
				tabBotones[i][j].setForeground(Color.WHITE);
				tabBotones[i][j].setBounds(ejeX, ejeY, dimensionBoton, dimensionBoton);
				panelLuces.add(tabBotones[i][j]);
				if(Presenter.estadoPosicion(i, j)){					
					tabBotones[i][j].setBackground(Color.RED);
				}else{
					tabBotones[i][j].setBackground(Color.GRAY);
				}
				ejeX+=espacioEntreBotones;
			}
			ejeY+=espacioEntreBotones;
		}
	}
	
	private void escucharBotones() {
		for (int i = 0; i < tabBotones.length; i++) {
			for (int j = 0; j < tabBotones.length; j++) {
				int x=i;
				int y=j;
				tabBotones[i][j].addActionListener(new ActionListener(){public void actionPerformed(ActionEvent arg0) {Presenter.actualizarTablero(x, y);actualizarColoresBotones();turnosDisponibles=turnosDisponibles-1;turnos.setText(String.valueOf(turnosDisponibles));}});	
			}
		}
		btnMezclar.setFont(new Font("Comic Sans MS", Font.PLAIN, 16));
		btnMezclar.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent arg0) {Presenter.mezclarTablero();;actualizarColoresBotones();turnosDisponibles=turnosDisponibles-1;turnos.setText(String.valueOf(turnosDisponibles));}});

	}
	
	private void escucharBotonVolver() {
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					frame.dispose();
					Menu.main(null);
				} 
				catch (Exception e) {JOptionPane.showMessageDialog(null, "ERROR SE ROMPIO");}		
			}		
		});
	}
	
	private void escucharBotonFinalizar() {
		btnFinalizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					if(Presenter.comprobarEstadoJuego()) {
						bloquearBotones();
						btnVolver.setEnabled(false);
						temporizador.start();
					}else {
						turnosDisponibles=1;
						verificarVictoriaDerrota();
						bloquearBotones();
						btnVolver.setEnabled(false);
						temporizador.start();
					}
				} 
				catch (Exception e) {JOptionPane.showMessageDialog(null, "ERROR SE ROMPIO");}		
			}		
		});
	}
	
	private void finalizarJuego() {
	    frame.dispose();
	}
	
	private void actualizarColoresBotones() {
		for (int i = 0; i < tamanioTablero; i++) {
			for (int j = 0; j  < tamanioTablero; j++) {
				if(Presenter.estadoPosicion(i, j)){					
					tabBotones[i][j].setBackground(Color.RED);
				}else{
					tabBotones[i][j].setBackground(Color.GRAY);
				}
			}
		}
		verificarVictoriaDerrota();
	}
	
	private void verificarVictoriaDerrota(){
		if(Presenter.comprobarEstadoJuego()){
			mostrarTablero();
			bloquearBotones();
			cambiarPaneles("GANASTE");
			}
		if(turnosDisponibles==1){
			mostrarTablero();
			bloquearBotones();
			cambiarPaneles("PERDISTE");
			}		
	}
	
	private void bloquearBotones() {
		for (int i = 0; i < tamanioTablero; i++) {
			for (int j = 0; j  < tamanioTablero; j++) {
				tabBotones[i][j].setEnabled(false);
			}
		}
		btnMezclar.setEnabled(false);
	}
	
	private void cambiarPaneles(String mensaje) {
		cartelFinal.setText(mensaje);
		panelFinal.setVisible(true);
		cartelFinal.setVisible(true);
		panelLuces.setVisible(false);
	}
		
	private void calcularDistancia() {
		dimensionBoton=60;
		espacioEntreBotones=dimensionBoton+10;
		anchoBotones=((tamanioTablero-1)*espacioEntreBotones)+dimensionBoton;
		anchoFrame=500;
		while(anchoBotones>anchoFrame) {
			dimensionBoton--;
			espacioEntreBotones=(int)((dimensionBoton*70)/60);
			anchoBotones=((tamanioTablero-1)*espacioEntreBotones)+dimensionBoton;
		}
		
		posicion=(anchoFrame/2-anchoBotones/2);	
	}
	

	/**
	 * Create the application.
	 */
	public Tablero() {
		initialize();
	    temporizador = new Timer(3000, new ActionListener() {
	        public void actionPerformed(ActionEvent e) {
	            temporizador.stop();
	            finalizarJuego();
	        }
	    });
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		inicializarPanelBotones();
		inicializarFrame();
		inicializarComponentes();
		agregarComponentes();
		inicializarPanelFinal();
		calcularDistancia();			
		mostrarTablero();
		escucharBotones();
		escucharBotonVolver();
		escucharBotonFinalizar();			
	}

}
